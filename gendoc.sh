#!/bin/bash
PREVDIR=`pwd`
cd "$(dirname $0)"

# Generate documentation
apidoc -i . -o ./doc/v1

# Go back where we came from
cd ${PREVDIR}
