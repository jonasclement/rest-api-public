<?php

namespace intellitech\models\v1;

class Locations extends BaseModel {

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var string
	 */
	public $adDN;

	/**
	 *
	 * @var integer
	 */
	public $createBy;

	/**
	 *
	 * @var string
	 */
	public $createTime;

	/**
	 * Initialize method for model.
	 */
	public function initialize() {

		$this->setSchema( "intellitech" );
		$this->setSource( "Locations" );
	}

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {

		return 'Locations';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return Locations[]|Locations|\Phalcon\Mvc\Model\ResultSetInterface
	 */
	public static function find( $parameters = null ) {

		return parent::find( $parameters );
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return Locations|\Phalcon\Mvc\Model\ResultInterface
	 */
	public static function findFirst( $parameters = null ) {

		return parent::findFirst( $parameters );
	}

}
