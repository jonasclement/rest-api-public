<?php

namespace intellitech\models\v1;

use Phalcon\Mvc\Model;

/**
 * @author  Jonas Clément <contact@jonasclement.dk>
 * @desc    Base database model
 *
 * BaseModel class
 */
class BaseModel extends Model {

	/**
	 * @desc   Extended find function to handle limit and offset "0" as none, rather than actually 0.
	 *
	 * @param null $parameters
	 *
	 * @return \Phalcon\Mvc\Model\ResultsetInterface
	 */
	public static function find( $parameters = null ) {

		if( is_array( $parameters ) ) {

			if( isset( $parameters['limit'] ) && $parameters['limit'] == 0 )
				unset( $parameters['limit'] );

			if( isset( $parameters['offset'] ) && $parameters['offset'] == 0 )
				unset( $parameters['offset'] );
		}

		return parent::find( $parameters );
	}
}
