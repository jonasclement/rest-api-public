<?php

namespace intellitech\models\v1;

class LoginAttempts extends BaseModel {

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var integer
	 */
	public $userID;

	/**
	 *
	 * @var string
	 */
	public $username;

	/**
	 *
	 * @var string
	 */
	public $sourceIP;

	/**
	 *
	 * @var string
	 */
	public $sessionID;

	/**
	 *
	 * @var integer
	 */
	public $success;

	/**
	 *
	 * @var string
	 */
	public $createTime;

	/**
	 * Initialize method for model.
	 */
	public function initialize() {

		$this->setSchema( "intellitech" );
		$this->setSource( "LoginAttempts" );
		$this->hasMany( 'id', 'intellitech\models\v1\UserSessions', 'loginAttemptID', [ 'alias' => 'Usersessions' ] );
	}

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {

		return 'LoginAttempts';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return LoginAttempts[]|LoginAttempts|\Phalcon\Mvc\Model\ResultSetInterface
	 */
	public static function find( $parameters = null ) {

		// Override default order
		if( !isset( $parameters['order'] ) ) $parameters['order'] = 'id DESC';
		return parent::find( $parameters );
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return LoginAttempts|\Phalcon\Mvc\Model\ResultInterface
	 */
	public static function findFirst( $parameters = null ) {

		return parent::findFirst( $parameters );
	}

}
