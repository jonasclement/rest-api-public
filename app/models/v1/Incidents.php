<?php

namespace intellitech\models\v1;

class Incidents extends BaseModel {

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var integer
	 */
	public $locationID;

	/**
	 *
	 * @var string
	 */
	public $headline;

	/**
	 *
	 * @var integer
	 */
	public $createBy;

	/**
	 *
	 * @var string
	 */
	public $createTime;

	/**
	 *
	 * @var integer
	 */
	public $changeBy;

	/**
	 *
	 * @var string
	 */
	public $changeTime;

	/**
	 * Initialize method for model.
	 */
	public function initialize() {

		$this->setSchema( "intellitech" );
		$this->setSource( "Incidents" );
		$this->belongsTo( 'locationID', 'intellitech\models\v1\Locations', 'id', [ 'alias' => 'Locations' ] );
		$this->hasMany( 'id', 'intellitech\models\v1\IncidentLines', 'incidentID', [ 'alias' => 'Incidentlines' ] );
		$this->belongsTo( 'createBy', 'intellitech\models\v1\Users', 'id', [ 'alias' => 'Users' ] );
	}

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {

		return 'Incidents';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed  $parameters
	 * @param string $order
	 *
	 * @return Incidents[]|Incidents|\Phalcon\Mvc\Model\ResultSetInterface
	 */
	public static function find( $parameters = null ) {

		// Override default order
		if( !isset( $parameters['order'] ) ) $parameters['order'] = 'id DESC';
		return parent::find( $parameters );
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return Incidents|\Phalcon\Mvc\Model\ResultInterface
	 */
	public static function findFirst( $parameters = null ) {

		return parent::findFirst( $parameters );
	}

}
