<?php

namespace intellitech\models\v1;

class Users extends BaseModel {

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var string
	 */
	public $adDN;

	/**
	 *
	 * @var integer
	 */
	public $createBy;

	/**
	 *
	 * @var string
	 */
	public $createTime;

	/**
	 * Initialize method for model.
	 */
	public function initialize() {

		$this->setSchema( "intellitech" );
		$this->setSource( "Users" );
		$this->hasMany( 'id', 'intellitech\models\v1\APIApplications', 'createBy', [ 'alias' => 'Apiapplications' ] );
		$this->hasMany( 'id', 'intellitech\models\v1\IncidentLines', 'createBy', [ 'alias' => 'Incidentlines' ] );
		$this->hasMany( 'id', 'intellitech\models\v1\Incidents', 'createBy', [ 'alias' => 'Incidents' ] );
		$this->hasMany( 'id', 'intellitech\models\v1\IncidentStatus', 'createBy', [ 'alias' => 'Incidentstatus' ] );
		$this->hasMany( 'id', 'intellitech\models\v1\Users', 'createBy', [ 'alias' => 'Users' ] );
		$this->hasMany( 'id', 'intellitech\models\v1\UserSessions', 'userID', [ 'alias' => 'Usersessions' ] );
		$this->belongsTo( 'createBy', 'intellitech\models\v1\Users', 'id', [ 'alias' => 'Users' ] );
	}

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {

		return 'Users';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return Users[]|Users|\Phalcon\Mvc\Model\ResultSetInterface
	 */
	public static function find( $parameters = null ) {

		return parent::find( $parameters );
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return Users|\Phalcon\Mvc\Model\ResultInterface
	 */
	public static function findFirst( $parameters = null ) {

		return parent::findFirst( $parameters );
	}

}
