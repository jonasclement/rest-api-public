<?php

namespace intellitech\models\v1;

class IncidentStatus extends BaseModel {

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var string
	 */
	public $name;

	/**
	 *
	 * @var string
	 */
	public $severityLevel;

	/**
	 *
	 * @var integer
	 */
	public $showOnWeb;

	/**
	 *
	 * @var integer
	 */
	public $createBy;

	/**
	 *
	 * @var string
	 */
	public $createTime;

	/**
	 *
	 * @var integer
	 */
	public $changeBy;

	/**
	 *
	 * @var string
	 */
	public $changeTime;

	/**
	 * Initialize method for model.
	 */
	public function initialize() {

		$this->setSchema( "intellitech" );
		$this->setSource( "IncidentStatus" );
		$this->hasMany( 'id', 'intellitech\models\v1\IncidentLines', 'incidentStatusID', [ 'alias' => 'Incidentlines' ] );
		$this->belongsTo( 'createBy', 'intellitech\models\v1\Users', 'id', [ 'alias' => 'Users' ] );
	}

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {

		return 'IncidentStatus';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return IncidentStatus[]|IncidentStatus|\Phalcon\Mvc\Model\ResultSetInterface
	 */
	public static function find( $parameters = null ) {

		return parent::find( $parameters );
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return IncidentStatus|\Phalcon\Mvc\Model\ResultInterface
	 */
	public static function findFirst( $parameters = null ) {

		return parent::findFirst( $parameters );
	}

}
