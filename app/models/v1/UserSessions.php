<?php

namespace intellitech\models\v1;

class UserSessions extends BaseModel {

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var integer
	 */
	public $userID;

	/**
	 *
	 * @var integer
	 */
	public $loginAttemptID;

	/**
	 *
	 * @var string
	 */
	public $selector;

	/**
	 *
	 * @var string
	 */
	public $token;

	/**
	 *
	 * @var string
	 */
	public $expires;

	/**
	 *
	 * @var string
	 */
	public $createTime;

	/**
	 * Initialize method for model.
	 */
	public function initialize() {

		$this->setSchema( "intellitech" );
		$this->setSource( "UserSessions" );
		$this->belongsTo( 'userID', 'intellitech\models\v1\Users', 'id', [ 'alias' => 'Users' ] );
		$this->belongsTo( 'loginAttemptID', 'intellitech\models\v1\LoginAttempts', 'id', [ 'alias' => 'Loginattempts' ] );
	}

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {

		return 'UserSessions';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return UserSessions[]|UserSessions|\Phalcon\Mvc\Model\ResultSetInterface
	 */
	public static function find( $parameters = null ) {

		// Override default order
		if( !isset( $parameters['order'] ) ) $parameters['order'] = 'id DESC';
		return parent::find( $parameters );
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return UserSessions|\Phalcon\Mvc\Model\ResultInterface
	 */
	public static function findFirst( $parameters = null ) {

		return parent::findFirst( $parameters );
	}

}
