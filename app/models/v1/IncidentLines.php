<?php

namespace intellitech\models\v1;

class IncidentLines extends BaseModel {

	/**
	 *
	 * @var integer
	 */
	public $id;

	/**
	 *
	 * @var integer
	 */
	public $incidentID;

	/**
	 *
	 * @var integer
	 */
	public $incidentStatusID;

	/**
	 *
	 * @var string
	 */
	public $message;

	/**
	 *
	 * @var integer
	 */
	public $createBy;

	/**
	 *
	 * @var string
	 */
	public $createTime;

	/**
	 *
	 * @var integer
	 */
	public $changeBy;

	/**
	 *
	 * @var string
	 */
	public $changeTime;

	/**
	 * Initialize method for model.
	 */
	public function initialize() {

		$this->setSchema( "intellitech" );
		$this->setSource( "IncidentLines" );
		$this->belongsTo( 'createBy', 'intellitech\models\v1\Users', 'id', [ 'alias' => 'Users' ] );
		$this->belongsTo( 'incidentID', 'intellitech\models\v1\Incidents', 'id', [ 'alias' => 'Incidents' ] );
		$this->belongsTo( 'incidentStatusID', 'intellitech\models\v1\IncidentStatus', 'id', [ 'alias' => 'Incidentstatus' ] );
	}

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {

		return 'IncidentLines';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return IncidentLines[]|IncidentLines|\Phalcon\Mvc\Model\ResultSetInterface
	 */
	public static function find( $parameters = null ) {

		// Override default order
		if( !isset( $parameters['order'] ) ) $parameters['order'] = 'id DESC';
		return parent::find( $parameters );
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 *
	 * @return IncidentLines|\Phalcon\Mvc\Model\ResultInterface
	 */
	public static function findFirst( $parameters = null ) {

		return parent::findFirst( $parameters );
	}

}
