<?php

namespace intellitech\models\v1;

class APIApplications extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $appID;

    /**
     *
     * @var string
     */
    public $secret;

    /**
     *
     * @var integer
     */
    public $allowUse;

    /**
     *
     * @var integer
     */
    public $createBy;

    /**
     *
     * @var string
     */
    public $createTime;

    /**
     *
     * @var integer
     */
    public $changeBy;

    /**
     *
     * @var string
     */
    public $changeTime;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("intellitech");
        $this->setSource("APIApplications");
        $this->belongsTo('createBy', 'intellitech\models\v1\Users', 'id', ['alias' => 'Users']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'APIApplications';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return APIApplications[]|APIApplications|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return APIApplications|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
