<?php

use intellitech\REST\Exceptions\v1\BadRequestException;
use intellitech\REST\Exceptions\v1\NamespaceNotFoundException;
use intellitech\REST\Exceptions\v1\RouteNotFoundException;
use intellitech\REST\Responses\v1\EmptyResponse;
use intellitech\lib\v1\ContentTypes;
use intellitech\lib\versions\VersionFactory;
use intellitech\lib\v1\Namespaces;

/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

/**
 * Request validation goes here
 */
// AJAX sends an OPTIONS request to figure out if it's allowed to talk to the server, so we need to handle that first.
if( $app->request->isOptions() ) {

	$response = new EmptyResponse( 204 );
	$response->setHeader( 'Access-Control-Allow-Origin', '*' );
	$response->setHeader( 'Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token-Selector, X-Auth-Token-Hash, X-App-ID, X-Sign' );
	$response->setHeader( 'Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE, OPTIONS' );
	$response->send();
	die;
}

// JSON is the only supported content type for now and we want it set explicitly
if( strpos( $app->request->getHeader( 'Content-Type' ), ContentTypes::JSON ) !== 0 )
	throw new BadRequestException( 'The supplied Content-Type is invalid. Please consult the documentation.' );

// Validate version
$version = VersionFactory::createFromUrl( $app->request->getURI() );
$vNum    = $version->getVersionNumber();

// Validate namespace
if( Namespaces::validateFromUrl( $app->request->getURI() ) === false )
	throw new NamespaceNotFoundException();

$ns = Namespaces::getFromUrl( $app->request->getURI() );

// Restricted and Secret endpoints require authentication
switch( $ns ) {
	case Namespaces::OPEN:
		break;
	case Namespaces::RESTRICTED:
	case Namespaces::SECRET:

		require_once APP_PATH . "/intellitech/lib/v{$vNum}/security.php";
		break;
}

/**
 * Routes go here
 */
if( $app->request->isGet() )
	require_once APP_PATH . "/intellitech/REST/routes/v{$vNum}/{$ns}/get.php";

elseif( $app->request->isPost() ) {

	$requestBody = $app->request->getJsonRawBody( true ) ?? [];

	foreach( $requestBody as $key => $value )
		$_POST[$key] = $value;

	// No need to continue processing an empty body as you would never POST without it...
	if( count( $_POST ) == 0 )
		throw new BadRequestException( 'Missing request body' );

	require_once APP_PATH . "/intellitech/REST/routes/v{$vNum}/{$ns}/post.php";
}
elseif( $app->request->isPatch() ) {

	$requestBody = $app->request->getJsonRawBody( true ) ?? [];

	foreach( $requestBody as $key => $value )
		$_POST[$key] = $value;

	// No need to continue processing an empty body as you would never PATCH without it...
	if( count( $_POST ) == 0 )
		throw new BadRequestException( 'Missing request body' );

	require_once APP_PATH . "/intellitech/REST/routes/v{$vNum}/{$ns}/patch.php";
}
elseif( $app->request->isDelete() ) {

	$requestBody = $app->request->getJsonRawBody( true ) ?? [];

	foreach( $requestBody as $key => $value )
		$_POST[$key] = $value;

	require_once APP_PATH . "/intellitech/REST/routes/v{$vNum}/{$ns}/delete.php";
}

/**
 * Not found handler
 */
$app->notFound(
	function() {

		throw new RouteNotFoundException();
	}
);
