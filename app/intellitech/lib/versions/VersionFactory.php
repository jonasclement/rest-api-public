<?php

namespace intellitech\lib\versions;

use intellitech\REST\Exceptions\v1\BadRequestException;

/**
 * @author  Jonas Clément <contact@jonasclement.dk>
 * @desc    Creates the correct version class from given data.
 *
 * VersionFactory class
 * @package intellitech\lib\versions
 */
abstract class VersionFactory {

	/**
	 * @desc   Creates the
	 *
	 * @param int $versionNumber
	 *
	 * @return VersionInterface
	 * @throws BadRequestException
	 */
	public static function create( int $versionNumber ): VersionInterface {

		switch( $versionNumber ) {

			case 1:
				return new Version1();
				break;
		}

		throw new BadRequestException( 'Invalid version number' );
	}

	public static function createFromUrl( string $url ): VersionInterface {

		$parts = explode( '/', $url );
		foreach( $parts as $part ) {

			if( preg_match( '/^v([0-9]+)$/', $part, $matches ) ) {

				$version = $matches[1];
				continue;
			}
		}

		return self::create( $version ?? -1 );
	}
}
