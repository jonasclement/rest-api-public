<?php

namespace intellitech\lib\versions;

/**
 * @author  Jonas Clément <contact@jonasclement.dk>
 * @desc    Interface for Version classes to implement.
 *
 * Interface VersionInterface
 * @package intellitech\lib\versions
 */
interface VersionInterface {

	/**
	 * @desc Returns the version number as an integer.
	 * @return int
	 */
	function getVersionNumber(): int;

	/**
	 * @desc Returns the base prefix for routes in the version.
	 * @return string
	 */
	function getBasePrefix(): string;
}
