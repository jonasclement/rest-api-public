<?php

namespace intellitech\lib\versions;

/**
 * @author  Jonas Clément <contact@jonasclement.dk>
 * @desc    Class for representing v1 of the API
 *
 * Version1 class
 * @package intellitech\lib\versions
 */
class Version1 implements VersionInterface {

	public function getVersionNumber(): int {

		return 1;
	}

	public function getBasePrefix(): string {

		return '/api/v1';
	}
}
