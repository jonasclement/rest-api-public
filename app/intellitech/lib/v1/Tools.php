<?php

namespace intellitech\lib\v1;

use Adldap\Models\User;
use intellitech\models\v1 as models;
use intellitech\REST\Exceptions\v1\InternalServerErrorException;

/**
 * @author  Jonas Clément <contact@jonasclement.dk>
 * @desc    Various utility functions
 *
 * Tools class
 * @package intellitech\lib\v1
 */
abstract class Tools {

	/**
	 * @desc   Gets HTTP response message by code
	 *
	 * @param int $code
	 *
	 * @return string
	 */
	public static function getResponseDescription( int $code ): string {

		$codes = [
			// Informational 1xx
			100 => 'Continue',
			101 => 'Switching Protocols',
			// Success 2xx
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			// Redirection 3xx
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found', // 1.1
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			// 306 is deprecated but reserved
			307 => 'Temporary Redirect',
			// Client Error 4xx
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			422 => 'Unprocessable Entity',
			// Server Error 5xx
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
			509 => 'Bandwidth Limit Exceeded'
		];

		$result = (isset( $codes[$code] )) ? $codes[$code] : 'Unknown Status Code';

		return $result;
	}

	/**
	 * @desc Gets content types supported by the API
	 * @return array
	 */
	private static function getSupportedContentTypes(): array {

		return [ ContentTypes::JSON ];
	}

	/**
	 * @desc Checks whether a given content type is valid
	 *
	 * @param string $contentType
	 *
	 * @return bool
	 */
	public static function checkIfContentTypeIsValid( string $contentType ): bool {

		return in_array( $contentType, self::getSupportedContentTypes() );
	}

	/**
	 * @desc Creates a user session
	 *
	 * @param models\Users         $user
	 * @param User                 $adUser
	 * @param models\LoginAttempts $loginAttempt
	 *
	 * @return array
	 * @throws InternalServerErrorException
	 */
	public static function createNewUserSession( models\Users $user, User $adUser, models\LoginAttempts $loginAttempt ): array {

		$token = random_bytes( 33 );

		$session                 = new models\UserSessions();
		$session->userID         = $user->id;
		$session->loginAttemptID = $loginAttempt->id;
		$session->selector       = str_replace( '/', '+', base64_encode( random_bytes( 9 ) ) ); // Remove slashes because we use these in a URL...
		$session->token          = hash( 'sha256', $token );

		// Expire time is set depending on the user type - admins = 10 hours, normal users = 10 years
		if( self::checkIfAdUserIsAdmin( $adUser ) )
			$session->expires = date( 'Y-m-d H:i:s', strtotime( 'now + 10 hours' ) );
		else
			$session->expires = date( 'Y-m-d H:i:s', strtotime( 'now + 10 years' ) );

		if( $session->save() )
			return [ 'selector' => $session->selector, 'token' => $token, 'expires' => $session->expires ];
		else
			throw new InternalServerErrorException( 'Something went wrong while generating your session' );
	}

	/**
	 * @desc Checks whether the given AD user is an administrator
	 *
	 * @param User $adUser
	 *
	 * @return bool
	 */
	public static function checkIfAdUserIsAdmin( User $adUser ): bool {

		return $adUser->inGroup( 'fko-webadmins' );
	}

	/**
	 * @desc Generates a signature for an API request
	 *
	 * @param string $secret
	 * @param string $requestBody
	 *
	 * @return string
	 */
	public static function generateRequestSign( string $secret, string $requestBody ): string {

		$time = time();
		// Due to time constraints, we're not using time (that's an ironic comment...)
		return hash( 'sha256', "{$secret}{$requestBody}" );
		//return hash( 'sha256', "{$secret}{$requestBody}{$time}" );
	}
}
