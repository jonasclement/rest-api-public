<?php

namespace intellitech\lib\v1;

/**
 * @author  Jonas Clément <contact@jonasclement.dk>
 * @desc    Contains static variables corresponding to supported content types
 *
 * ContentTypes class
 * @package intellitech\lib
 */
class ContentTypes {

	public const JSON = 'application/json';
}
