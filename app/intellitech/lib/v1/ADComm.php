<?php

namespace intellitech\lib\v1;


use Adldap\Adldap;
use Adldap\Auth\BindException;
use Adldap\Auth\PasswordRequiredException;
use Adldap\Auth\UsernameRequiredException;
use intellitech\REST\Exceptions\v1\InternalServerErrorException;

/**
 * @author  Jonas Clément <contact@jonasclement.dk>
 * @desc    Facilitates communication with Active Directory.
 *
 * ADComm class
 * @package intellitech\lib\v1
 */
class ADComm {

	/**
	 * @var Adldap
	 */
	private $ad;

	private $provider;

	public function __construct() {

		try {

			$configFile = file_get_contents( APP_PATH . '/config/ad_config.json' );
			$jsonConfig = json_decode( $configFile, true );
		}
		catch( \Exception $e ) {

			throw new InternalServerErrorException( 'Internal error, AD JSON config missing' );
		}

		$config = [
			'hosts'    => $jsonConfig['hosts'],
			'base_dn'  => $jsonConfig['base_dn'],
			'username' => $jsonConfig['username'],
			'password' => $jsonConfig['password'],

		];

		$this->ad = new Adldap();
		$this->ad->addProvider( $config );
		$this->provider = $this->ad->connect();
	}

	/**
	 * @desc Authenticates the given username and password with the AD
	 *
	 * @param string $username
	 * @param string $password
	 *
	 * @return bool
	 * @throws InternalServerErrorException
	 * @throws UsernameRequiredException
	 */
	public function authenticate( string $username, string $password ): bool {

		$username = "{$username}@fko.local";
		try {

			return $this->provider->auth()->attempt( $username, $password );
		}
		catch( BindException $e ) {

			throw new InternalServerErrorException( 'BindException' );
		}
		catch( PasswordRequiredException $e ) {

			throw new InternalServerErrorException( 'PasswordRequiredException' );
		}
		catch( UsernameRequiredException $e ) {

			throw new UsernameRequiredException( 'UsernameRequiredException' );
		}
	}

	/**
	 * @desc Gets an object by distinguished name
	 *
	 * @param string $dn
	 *
	 * @return \Adldap\Models\Model|array|false
	 */
	public function getDataByDN( string $dn ) {

		return $this->provider->search()
							  ->findBy( 'distinguishedName', $dn );
	}

	/**
	 * @desc Gets a user by distinguished name
	 *
	 * @param string $dn
	 *
	 * @return \Adldap\Models\User|array|false
	 */
	public function getUserDataByDn( string $dn ) {

		return $this->provider->search()
							  ->users()
							  ->findBy( 'distinguishedName', $dn );
	}

	/**
	 * @desc Gets an OU by distinguished name
	 *
	 * @param string $dn
	 *
	 * @return \Adldap\Models\OrganizationalUnit|array|false
	 */
	public function getOuDataByDN( string $dn ) {

		return $this->provider->search()
							  ->ous()
							  ->findBy( 'distinguishedName', $dn );
	}

	/**
	 * @desc Finds a user by the given username
	 *
	 * @param string $username
	 *
	 * @return \Adldap\Models\User|array|null
	 */
	public function getUserDataByUsername( string $username ) {

		$user = $this->provider->search()
							   ->users()
							   ->find( $username );

		return $user;
	}

	/**
	 * @desc Finds the location by the user DN
	 *
	 * @param string $dn
	 *
	 * @return \Adldap\Models\OrganizationalUnit|array|false
	 */
	public function getLocationByUserDN( string $dn ) {

		// Do a little fiddling to get the name of the user's location...
		$explodeDN = explode( ',', $dn );
		unset( $explodeDN[0] );
		$locationDN = implode( ',', $explodeDN );

		// Get location SID
		$location = $this->provider->search()
								   ->ous()
								   ->findBy( 'distinguishedName', $locationDN );

		return $location;
	}
}
