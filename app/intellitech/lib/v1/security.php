<?php

use intellitech\REST\Exceptions\v1\UnauthorizedException;
use intellitech\models\v1\UserSessions;
use intellitech\models\v1\APIApplications;
use intellitech\lib\v1\Namespaces;
use intellitech\lib\v1\ADComm;
use intellitech\lib\v1\Tools;

// Validate selector
$selector = $app->request->getHeader( 'X-Auth-Token-Selector' ) ?? '';
$token    = $app->request->getHeader( 'X-Auth-Token-Hash' ) ?? '';
if( empty( $selector ) || empty( $token ) )
	throw new UnauthorizedException( 'Missing X-Auth-Token headers' );

// Decode the token
$token = base64_decode( $token );

// Validate the token
$session = UserSessions::findFirstBySelector( $selector );
if( $session === false )
	throw new UnauthorizedException();

// Check request signing
$appID  = $app->request->getHeader( 'X-App-ID' ) ?? -1;
$sign   = $app->request->getHeader( 'X-Sign' ) ?? -1;
$apiApp = APIApplications::findFirstByAppID( $appID );
$body   = $app->request->getRawBody() ?? '';

if( $apiApp === false || empty( $sign ) )
	throw new UnauthorizedException( 'Missing request sign' );

if( $sign != Tools::generateRequestSign( $apiApp->secret, $body ) )
	throw new UnauthorizedException( 'Invalid request sign' );

// Check if the token exists
if( hash_equals( $session->token, hash( 'sha256', $token ) ) ) {

	// If the token is expired, delete it and throw unauthorized
	if( time() > strtotime( $session->expires ) ) {

		$session->delete();
		throw new UnauthorizedException();
	}

	// Good to go - set the create by user in session for easy access...
	$_SESSION['authUser'] = $session->Users;
}
else
	throw new UnauthorizedException();

// If the endpoint is SECRET, check whether the user is an administrator
if( $ns == Namespaces::SECRET ) {

	// Throw unauthorized if the user is not an admin
	$ad     = new ADComm();
	$adUser = $ad->getDataByDN( $_SESSION['authUser']->adDN );
	if( !Tools::checkIfAdUserIsAdmin( $adUser ) )
		throw new UnauthorizedException();
}
