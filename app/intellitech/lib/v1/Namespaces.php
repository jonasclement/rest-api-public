<?php

namespace intellitech\lib\v1;

/**
 * @author  Jonas Clément <contact@jonasclement.dk>
 * @desc    Contains static variables corresponding to API namespaces
 *
 * Namespaces class
 * @package intellitech\lib
 */
abstract class Namespaces {

	public const OPEN = 'open';
	public const RESTRICTED = 'restricted';
	public const SECRET = 'secret';

	/**
	 * @desc   Checks whether a given namespace is valid
	 *
	 * @param string $namespace Namespace to validate
	 *
	 * @return bool
	 */
	public static function validate( string $namespace ): bool {

		$validNamespaces = [ self::OPEN, self::RESTRICTED, self::SECRET ];

		return in_array( $namespace, $validNamespaces );
	}

	/**
	 * @desc   Finds the namespace in the given URL and validates it
	 *
	 * @param string $url URL to check
	 *
	 * @return bool
	 */
	public static function validateFromUrl( string $url ): bool {

		$namespace = self::getFromUrl( $url );
		if( !empty( $namespace ) )
			return self::validate( $namespace );
		else
			return false;
	}

	/**
	 * @desc Gets the namespace in the given URL
	 *
	 * @param string $url
	 *
	 * @return string
	 */
	public static function getFromUrl( string $url ): string {

		$validNamespaces = [
			self::OPEN,
			self::RESTRICTED,
			self::SECRET
		];

		// We know that the namespace must always come after the version and it only contains letters, so we can extract it like so:
		// Build pattern
		$pattern           = "/v[0-9+]\/([%s]+)\//";
		$namespacesPattern = '';
		foreach( $validNamespaces as $namespace )
			$namespacesPattern = "{$namespacesPattern}{$namespace}|";

		// Remove last pipe and replace %s in the pattern
		$namespacesPattern = substr( $namespacesPattern, 0, -1 );
		$pattern           = str_replace( '%s', $namespacesPattern, $pattern );

		if( preg_match( $pattern, $url, $matches ) ) {

			return $matches[1];
		}
		else
			return '';
	}
}
