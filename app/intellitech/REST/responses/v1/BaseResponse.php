<?php

namespace intellitech\REST\Responses\v1;

use Phalcon\Http\Response;
use intellitech\lib\v1\Tools;

/**
 * @author  Jonas Clément <contact@jonasclement.dk>
 * @desc    Base class for API responses. Different types can extend this class.
 *
 * BaseResponse class
 * @package intellitech\REST\Responses\v1
 */
abstract class BaseResponse {

	/**
	 * @var int
	 */
	private $httpStatusCode;

	/**
	 * @var string
	 */
	private $httpStatusMessage;

	/**
	 * @var string
	 */
	private $responseContentType;

	/**
	 * @var array
	 */
	private $headers = [];

	/**
	 * @var mixed
	 */
	protected $data;

	/**
	 * BaseResponse constructor.
	 *
	 * @param int    $httpStatusCode
	 * @param string $responseContentType
	 * @param mixed  $data
	 */
	public function __construct( int $httpStatusCode, string $responseContentType, $data = null ) {

		$this->setHttpStatusCode( $httpStatusCode );
		$this->responseContentType = $responseContentType;
		$this->data                = $data;
	}

	/**
	 * @return int
	 */
	public function getHttpStatusCode(): int {

		return $this->httpStatusCode;
	}

	/**
	 * @param int $httpStatusCode
	 *
	 * @return BaseResponse
	 */
	protected function setHttpStatusCode( int $httpStatusCode ): BaseResponse {

		$this->httpStatusCode    = $httpStatusCode;
		$this->httpStatusMessage = Tools::getResponseDescription( $httpStatusCode );
		return $this;
	}

	/**
	 * @return string
	 */
	public function getHttpStatusMessage(): string {

		return $this->httpStatusMessage;
	}

	/**
	 * @return string
	 */
	public function getResponseContentType(): string {

		return $this->responseContentType;
	}

	/**
	 * @return array
	 */
	public function getHeaders(): array {

		return $this->headers;
	}

	/**
	 * @desc Sets a HTTP header
	 *
	 * @param string $key
	 * @param string $value
	 */
	public function setHeader( string $key, string $value ) {

		$this->headers[$key] = $value;
	}

	/**
	 * @desc Removes the HTTP header with the given key
	 *
	 * @param string $key
	 */
	public function removeHeader( string $key ) {

		if( !empty( $this->headers[$key] ) )
			unset($this->headers[$key]);
	}

	/**
	 * @return mixed
	 */
	public function getData() {

		return $this->data;
	}

	/**
	 * @desc   Should be implemented by children.
	 * @return Response|void
	 */
	public function send() {

		throw new \BadMethodCallException( 'Class has not implemented send() function - bad developer!' );
	}
}
