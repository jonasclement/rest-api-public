<?php

namespace intellitech\REST\Responses\v1;

use Phalcon\Http\Response;
use intellitech\lib\v1\ContentTypes;

/**
 * @author  Jonas Clément <contact@jonasclement.dk>
 * @desc    Class for creating JSON responses.
 *
 * JSONResponse class
 * @package intellitech\REST\Responses\v1
 */
class JSONResponse extends BaseResponse {

	public function __construct( int $httpStatusCode, ?array $data = null ) {

		parent::__construct( $httpStatusCode, 'application/json', $data );
		$this->setHeader( 'Content-Type', ContentTypes::JSON );
	}

	/**
	 * @desc   Sends the response
	 * @return Response
	 */
	public function send(): Response {

		$response = new Response();
		$response->setStatusCode( $this->getHttpStatusCode(), $this->getHttpStatusMessage() );

		foreach( $this->getHeaders() as $key => $value )
			$response->setHeader( $key, $value );

		if( $this->data !== null )
			$response->setJsonContent( $this->data );

		return $response->send();
	}
}
