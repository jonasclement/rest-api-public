<?php

namespace intellitech\REST\Responses\v1;

use Phalcon\Http\Response;

/**
 * @author  Jonas Clément <contact@jonasclement.dk>
 * @desc    Class for sending responses with no body
 *
 * EmptyResponse class
 * @package intellitech\REST\Responses\v1
 */
class EmptyResponse extends BaseResponse {

	public function __construct( int $httpStatusCode ) {

		parent::__construct( $httpStatusCode, '' );
	}

	public function send(): Response {

		$response = new Response();
		$response->setStatusCode( $this->getHttpStatusCode(), $this->getHttpStatusMessage() );

		foreach( $this->getHeaders() as $key => $value )
			$response->setHeader( $key, $value );

		return $response->send();
	}
}
