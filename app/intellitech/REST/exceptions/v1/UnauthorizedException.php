<?php

namespace intellitech\REST\Exceptions\v1;

/**
 * @apiDefine BadRequestException
 * @apiError (Error 401 - Unauthorized) {Number} httpCode The HTTP response code of the request - always 401
 * @apiError (Error 401 - Unauthorized) {String} httpMessage The HTTP RFC2616 Description of the response code - always "Unauthorized"
 * @apiError (Error 401 - Unauthorized) {String} technicalErrorMessage A more detailed error message saying what went wrong.
 */
class UnauthorizedException extends BaseHttpException {
	protected $code = 401;

	public function __construct( string $technicalErrorMessage = 'You are not authorized to use this endpoint' ) {

		parent::__construct( $technicalErrorMessage );
	}
}
