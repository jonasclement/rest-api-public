<?php

namespace intellitech\REST\Exceptions\v1;

use Phalcon\Http\Response;
use intellitech\lib\v1\Tools;

/**
 * @author  Jonas Clément <contact@jonasclement.dk>
 * @desc    Base class for HTTP exceptions to use.
 *          What makes these special from other exceptions is the "send" method.
 * @see BaseHttpException::send()
 *
 * BaseHttpException class
 * @package intellitech\REST\Exceptions\v1
 */
abstract class BaseHttpException extends \Exception {
	/**
	 * @var int
	 */
	protected $code = 500;

	/**
	 * @var string
	 */
	protected $message = 'An error occurred while processing your request.';

	/**
	 * @var null|string
	 */
	protected $technicalErrorMessage;

	public function __construct( string $technicalErrorMessage ) {

		$this->technicalErrorMessage = $technicalErrorMessage;
	}

	/**
	 * @desc   Sends the exception as a Phalcon Response
	 */
	public function send() {

		$this->message = Tools::getResponseDescription( $this->code );

		$response = new Response();
		$response->setStatusCode( $this->code );
		$response->setJsonContent(
			[
				'httpCode'              => $this->code,
				'httpMessage'           => $this->message,
				'technicalErrorMessage' => $this->technicalErrorMessage
			]
		);

		$response->send();
		return;
	}
}
