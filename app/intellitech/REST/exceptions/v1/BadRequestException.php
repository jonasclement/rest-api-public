<?php

namespace intellitech\REST\Exceptions\v1;

/**
 * @apiDefine BadRequestException
 * @apiError (Error 400 - Bad Request) {Number} httpCode The HTTP response code of the request - always 400
 * @apiError (Error 400 - Bad Request) {String} httpMessage The HTTP RFC2616 Description of the response code - always "Bad Request"
 * @apiError (Error 400 - Bad Request) {String} technicalErrorMessage A more detailed error message saying what went wrong.
 */
class BadRequestException extends BaseHttpException {
	protected $code = 400;

	public function __construct( string $technicalErrorMessage = '' ) {

		parent::__construct( $technicalErrorMessage );
	}
}
