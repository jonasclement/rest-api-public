<?php

namespace intellitech\REST\Exceptions\v1;

class ObjectNotFoundException extends NotFoundException {

	public function __construct( string $object = 'object' ) {

		parent::__construct( "The requested {$object} could not be found" );
	}
}
