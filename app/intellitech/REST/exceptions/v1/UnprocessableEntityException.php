<?php

namespace intellitech\REST\Exceptions\v1;

/**
 * @apiDefine UnprocessableEntityException
 * @apiError (Error 422 - Unprocessable Entity) {Number} httpCode The HTTP response code of the request - always 422
 * @apiError (Error 422 - Unprocessable Entity) {String} httpMessage The HTTP RFC4918 Description of the response code - always "Unprocessable Entity"
 * @apiError (Error 422 - Unprocessable Entity) {String} technicalErrorMessage A more detailed error message saying what went wrong.
 */
class UnprocessableEntityException extends BaseHttpException {
	protected $code = 422;

	public function __construct( string $object = 'object' ) {

		parent::__construct( "Sub-object {$object} is invalid or does not exist" );
	}
}
