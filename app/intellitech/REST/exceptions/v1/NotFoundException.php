<?php

namespace intellitech\REST\Exceptions\v1;

/**
 * @apiDefine NotFoundException
 * @apiError (Error 404 - Not Found) {Number} httpCode The HTTP response code of the request - always 404
 * @apiError (Error 404 - Not Found) {String} httpMessage The HTTP RFC2616 Description of the response code - always "Not Found"
 * @apiError (Error 404 - Not Found) {String} technicalErrorMessage A more detailed error message saying what went wrong.
 */
class NotFoundException extends BaseHttpException {
	protected $code = 404;

	public function __construct( string $technicalErrorMessage = '' ) {

		parent::__construct( $technicalErrorMessage );
	}
}
