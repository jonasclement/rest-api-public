<?php

namespace intellitech\REST\Exceptions\v1;

class RouteNotFoundException extends BaseHttpException {
	protected $code = 404;

	public function __construct() {

		parent::__construct( 'The requested endpoint could not be found.' );
	}
}
