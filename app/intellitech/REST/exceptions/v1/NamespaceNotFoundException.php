<?php

namespace intellitech\REST\Exceptions\v1;

class NamespaceNotFoundException extends NotFoundException {

	public function __construct() {

		parent::__construct( "The requested namespace could not be found" );
	}
}
