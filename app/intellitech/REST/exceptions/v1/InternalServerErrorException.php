<?php

namespace intellitech\REST\Exceptions\v1;

/**
 * @apiDefine InternalServerErrorException
 * @apiError (Error 500 - Bad Request) {Number} httpCode The HTTP response code of the request - always 500
 * @apiError (Error 500 - Bad Request) {String} httpMessage The HTTP RFC2616 Description of the response code - always "Internal Server Error"
 * @apiError (Error 500 - Bad Request) {String} technicalErrorMessage A more detailed error message saying what went wrong.
 */
class InternalServerErrorException extends BaseHttpException {
	protected $code = 500;

	public function __construct( string $technicalErrorMessage = '' ) {

		parent::__construct( $technicalErrorMessage );
	}
}
