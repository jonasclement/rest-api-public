<?php

use intellitech\REST\Responses\v1\JSONResponse;
use intellitech\lib\v1\Namespaces;
use intellitech\models\v1 as models;
use intellitech\REST\Exceptions\v1 as exceptions;
use intellitech\lib\v1\ADComm;
use intellitech\lib\v1\Tools;

// Namespaces for building routes
$nsOpen       = Namespaces::OPEN;
$nsRestricted = Namespaces::RESTRICTED;
$nsSecret     = Namespaces::SECRET;

/**
 * @api {get} v1/restricted/user Logged-in user
 * @apiName UserBySession
 * @apiGroup Users
 * @apiVersion 1.0.0
 * @apiPermission Restricted
 *
 * @apiDescription
 *
 * Returns an object containing user data.
 *
 * @apiSuccess {Object} user User object
 * @apiSuccess {Number} user.id User identifier
 * @apiSuccess {Number} user.locationID User location identifier
 * @apiSuccess {String} user.username User's username
 * @apiSuccess {String} user.displayName User's display name,
 * @apiSuccess {Boolean} user.isAdmin Whether the user is an administrator or not
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
    "user": {
        "locationID": 1,
        "username": "fko-user01",
        "displayName": "fko normaluser 01",
        "isAdmin": false
    }
}
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsRestricted}/user", function() {

	$responseData = [];

	// Get AD data
	$adComm         = new ADComm();
	$userAdData     = $adComm->getDataByDN( $_SESSION['authUser']->adDN );
	$locationAdData = $adComm->getLocationByUserDN( $_SESSION['authUser']->adDN );

	$location = models\Locations::findFirstByAdDN( $locationAdData->getDn() );

	$response = new JSONResponse(
		200,
		[
			'user' => [
				'id'          => $_SESSION['authUser']->id,
				'locationID'  => $location->id ?? null,
				'username'    => $userAdData->getAccountName(),
				'displayName' => $userAdData->getDisplayName(),
				'isAdmin'     => Tools::checkIfAdUserIsAdmin( $userAdData )
			]
		]
	);
	$response->send();
}
);
