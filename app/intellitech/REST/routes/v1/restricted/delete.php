<?php

use intellitech\REST\Responses\v1\EmptyResponse;
use intellitech\lib\v1\Namespaces;
use intellitech\models\v1 as models;
use intellitech\REST\Exceptions\v1 as exceptions;

// Namespaces for building routes
$nsOpen       = Namespaces::OPEN;
$nsRestricted = Namespaces::RESTRICTED;
$nsSecret     = Namespaces::SECRET;

/**
 * @api {delete} v1/restricted/user-sessions/<selector> Delete user session
 * @apiName DeleteUserSession
 * @apiGroup Users
 * @apiVersion 1.0.0
 * @apiPermission Restricted
 *
 * @apiDescription
 *
 * Delete a user session, effectively logging the user out of that instance.
 *
 * @apiSuccessExample {json} Success-204-Example
HTTP/1.1 204 No Content
 *
 * @apiUse NotFoundException
 */
$app->delete(
	"{$version->getBasePrefix()}/{$nsRestricted}/user-sessions/{selector}", function( $selector ) use ( $app ) {

	// The selector isn't actually necessary in the URL as it is also sent in headers,
	// but the URL wouldn't be REST-compliant if it wasn't there.

	$headerSelector = $app->request->getHeader( 'X-Auth-Token-Selector' );
	$headerSession = models\UserSessions::findFirstBySelector( $headerSelector );
	$requestedSession = models\UserSessions::findFirstBySelector( $selector );

	// Check if the requested session even exists
	if( $requestedSession !== false ) {

		// Check that the selector is owned by the user TODO or an admin?
		if( $headerSession->userID == $requestedSession->userID ) {

			if( $requestedSession->delete() ) {

				$response = new EmptyResponse( '204' );
				$response->send();
			}
			else
				throw new exceptions\InternalServerErrorException( 'Something went wrong while logging out' );
		}
		else
			throw new exceptions\ObjectNotFoundException( 'token' ); // Intentionally vague to hide whether the token was valid or not
	}
	else
		throw new exceptions\ObjectNotFoundException( 'token' ); // Intentionally vague to hide whether the token was valid or not
}
);
