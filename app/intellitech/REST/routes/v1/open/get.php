<?php

use intellitech\REST\Responses\v1\JSONResponse;
use intellitech\lib\v1\Namespaces;
use intellitech\models\v1 as models;
use intellitech\REST\Exceptions\v1 as exceptions;
use intellitech\lib\v1\ADComm;

// Namespaces for building routes
$nsOpen       = Namespaces::OPEN;
$nsRestricted = Namespaces::RESTRICTED;
$nsSecret     = Namespaces::SECRET;

/**
 * @api {get} v1/open/client-ip Get client IP
 * @apiName ClientIP
 * @apiGroup ClientInfo
 * @apiVersion 1.0.0
 * @apiPermission Open
 *
 * @apiDescription
 *
 * Returns an object containing the IP address of the client calling the endpoint.
 * Effectively, this endpoint also works as a "ping".
 *
 * @apiSuccess {Object} ip IP object
 * @apiSuccess {String} ip.address IP address
 *
 * @apiSuccessExample {json} Success-200-Example
 *
HTTP/1.1 200 OK
{
    "ip": {
        "address": "127.0.0.1"
    }
}
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsOpen}/client-ip", function() {

	$responseData = [ 'ip' => [] ];

	$responseData['ip']['address'] = $_SERVER['REMOTE_ADDR'];

	$response = new JSONResponse( 200, $responseData );
	return $response->send();
}
);

/**
 * @api {get} v1/open/incidents List of incidents
 * @apiName IncidentList
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Open
 *
 * @apiDescription
 *
 * Returns an array of containing all incidents.
 *
 * @apiSuccess {Array} incidents Incident list
 * @apiSuccess {Number} incidents.id Incident identifier
 * @apiSuccess {Number} incidents.locationID Location identifier
 * @apiSuccess {String} incidents.headline Headline for the incident
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
	"incidents": [
        {
            "id": 2,
            "locationID": 2,
            "headline": "Nedbrud hos vores ISP",
            "createBy": 1,
            "createTime": "2018-10-18 20:49:50",
            "changeBy": null,
            "changeTime": null,
        },
        {
            "id": 1,
            "locationID": 1,
            "headline": "Fejl på central switch",
            "createBy": 1,
            "createTime": "2018-10-18 20:49:50",
            "changeBy": null,
            "changeTime": null,
        }
    ]
}
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsOpen}/incidents", function() use ( $app ) {

	$responseData = [];

	$limit    = $app->request->getQuery( 'limit', null, '0' );
	$offset   = $app->request->getQuery( 'offset', null, '0' );
	$fromTime = date( 'Y-m-d H:i:s', $app->request->getQuery( 'from-time', null, '0' ) );
	$toTime   = date( 'Y-m-d H:i:s', $app->request->getQuery( 'to-time', null, time() ) );

	$incidents = models\Incidents::find(
		[
			'conditions' => 'createTime BETWEEN ?1 AND ?2',
			'bind'       => [
				1 => $fromTime,
				2 => $toTime
			],
			'limit'      => $limit,
			'offset'     => $offset
		]
	);
	if( $incidents !== false ) {

		$total                     = models\Incidents::count(
			[
				'conditions' => 'createTime BETWEEN ?1 AND ?2',
				'bind'       => [
					1 => $fromTime,
					2 => $toTime
				]
			]
		);
		$responseData['_meta']     = [ 'itemCount' => count( $incidents ), 'itemsTotal' => $total ];
		$responseData['incidents'] = $incidents;
	}
	else {

		$responseData['_meta']     = [ 'itemCount' => 0, 'itemsTotal' => 0 ];
		$responseData['incidents'] = [];
	}

	$response = new JSONResponse( 200, $responseData );
	$response->send();
}
);

/**
 * @api {get} v1/open/incidents/<id> Specific incident
 * @apiName Incident
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Open
 *
 * @apiDescription
 *
 * Returns an object containing the requested incident
 *
 * @apiSuccess {Object} incident Incident
 * @apiSuccess {Number} incidents.id Incident identifier
 * @apiSuccess {Number} incidents.locationID Location identifier
 * @apiSuccess {String} incidents.headline Headline for the incident
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
    "incident": {
        "id": 1,
        "headline": "Fejl på central switch",
        "locationID": 1,
        "createBy": 1,
        "createTime": "2018-10-18 20:49:50",
        "changeBy": null,
        "changeTime": null
    }
}
 *
 * @apiUse NotFoundException
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsOpen}/incidents/{id:[0-9]+}", function( $id ) {

	$responseData = [];

	$incident = models\Incidents::findFirst( $id );
	if( $incident !== false )
		$responseData['incident'] = $incident;
	else
		throw new exceptions\ObjectNotFoundException( 'incident' );

	$response = new JSONResponse( 200, $responseData );
	$response->send();
}
);

/**
 * @api {get} v1/open/incidents/<incident-id>/messages List of incident messages
 * @apiName IncidentMessageList
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Open
 *
 * @apiDescription
 *
 * Returns an array of containing all incident messages for the requested incident.
 *
 * @apiSuccess {Array} messages Message list
 * @apiSuccess {Number} messages.id Incident message identifier
 * @apiSuccess {Number} messages.incidentID Incident identifier
 * @apiSuccess {Number} messages.incidentStatusID Incident status identifier
 * @apiSuccess {String} messages.message The message
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
    "messages": [
        {
            "id": 2,
            "incidentID": 2,
            "incidentStatusID": 3,
            "message": "Problemerne er nu løst",
            "createBy": 1,
            "createTime": "2018-10-18 11:05:43",
            "changeBy": null,
            "changeTime": null
        },
        {
            "id": 1,
            "incidentID": 2,
            "incidentStatusID": 1,
            "message": "Forbindelsen til vores ISP er gået ned, og alle står derfor uden internetforbindelse.",
            "createBy": 1,
            "createTime": "2018-10-18 08:50:22",
            "changeBy": null,
            "changeTime": null
        }
    ]
}
 *
 * @apiUse NotFoundException
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsOpen}/incidents/{incidentID:[0-9]+}/messages", function( $incidentID ) {

	$responseData = [];

	$incident = models\Incidents::findFirst( $incidentID );
	if( $incident !== false )
		$responseData['messages'] = $incident->IncidentLines;
	else
		throw new exceptions\ObjectNotFoundException( 'incident' );

	$response = new JSONResponse( 200, $responseData );
	$response->send();
}
);

/**
 * @api {get} v1/open/incidents/<incident-id>/messages/<id> Specific incident message
 * @apiName IncidentMessage
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Open
 *
 * @apiDescription
 *
 * Returns an object containing the requested incident message.
 *
 * @apiSuccess {Object} message Message object
 * @apiSuccess {Number} message.id Incident message identifier
 * @apiSuccess {Number} message.incidentID Incident identifier
 * @apiSuccess {Number} message.incidentStatusID Incident status identifier
 * @apiSuccess {String} message.message The message
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
    "message": {
        "id": 2,
        "incidentID": 1,
        "incidentStatusID": 3,
        "message": "Problemerne er nu løst",
        "createBy": 1,
        "createTime": "2018-10-18 20:50:22",
        "changeBy": null,
        "changeTime": null
    }
}
 *
 * @apiUse NotFoundException
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsOpen}/incidents/{incidentID:[0-9]+}/messages/{id:[0-9]+}", function( $incidentID, $id ) {

	$responseData = [];

	$incident = models\Incidents::findFirst( $incidentID );
	if( $incident !== false ) {

		$message = models\IncidentLines::findFirst( $id );
		if( $message !== false && $message->incidentID == $incident->id )
			$responseData['message'] = $message;
		else
			throw new exceptions\ObjectNotFoundException( 'incident message' );
	}
	else
		throw new exceptions\ObjectNotFoundException( 'incident' );

	$response = new JSONResponse( 200, $responseData );
	$response->send();
}
);

/**
 * @api {get} v1/open/locations List of locations
 * @apiName LocationList
 * @apiGroup Locations
 * @apiVersion 1.0.0
 * @apiPermission Open
 *
 * @apiDescription
 *
 * Returns an array of containing all locations.
 *
 * @apiSuccess {Array} locations Location list
 * @apiSuccess {Number} locations.id Location identifier
 * @apiSuccess {String} locations.name Name of the location
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
    "locations": [
        {
            "id": 1,
            "name": "Location01"
        },
        {
            "id": 2,
            "name": "Location02"
        }
    ]
}
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsOpen}/locations", function() use ( $app ) {

	$responseData = [];

	$limit  = $app->request->getQuery( 'limit', null, '0' );
	$offset = $app->request->getQuery( 'offset', null, '0' );

	$locations = models\Locations::find( [ 'limit' => $limit, 'offset' => $offset ] );
	if( $locations !== false ) {

		$total                 = models\Users::count();
		$responseData['_meta'] = [ 'itemCount' => count( $locations ), 'itemsTotal' => $total ];
		$responseData['locations'] = [];

		$adComm = new ADComm();
		foreach( $locations as $location ) {

			// Fetch AD data
			$locationAdData = $adComm->getOuDataByDN( $location->adDN );

			$responseRow = [
				'id'          => $location->id,
				'name' => $locationAdData->getOu()
			];

			$responseData['locations'][] = $responseRow;
		}

		$response = new JSONResponse( 200, $responseData );
		$response->send();
	}
	else {

		$responseData['_meta'] = [ 'itemCount' => 0, 'itemsTotal' => 0 ];
		$responseData['locations'] = [];
	}
}
);

/**
 * @api {get} v1/open/locations/<id> Location
 * @apiName Location
 * @apiGroup Locations
 * @apiVersion 1.0.0
 * @apiPermission Open
 *
 * @apiDescription
 *
 * Returns an object containing the requested location.
 *
 * @apiSuccess {Object} location Location object
 * @apiSuccess {Number} location.id Location identifier
 * @apiSuccess {String} location.name Name of the location
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
    "location": {
        "id": 2,
        "name": "Location02"
    }
}
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsOpen}/locations/{id:[0-9]+}", function( $id ) {

	$responseData = [];

	$location = models\Locations::findFirst( $id );
	if( $location !== false ) {

		$responseData['location'] = [];

		$adComm = new ADComm();

			// Fetch AD data
			$locationAdData = $adComm->getOuDataByDN( $location->adDN );

			$responseData['location'] = [
				'id'          => $location->id,
				'name' => $locationAdData->getOu()
			];

		$response = new JSONResponse( 200,$responseData );
		$response->send();
	}
	else {

		throw new exceptions\ObjectNotFoundException( 'location' );
	}
}
);

/**
 * @api {get} v1/open/locations/<location-id>/incidents List of incidents by location
 * @apiName IncidentListByLocation
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Open
 *
 * @apiDescription
 *
 * Returns an array of containing all incidents at the given location.
 *
 * @apiSuccess {Array} incidents Incident list
 * @apiSuccess {Number} incidents.id Incident identifier
 * @apiSuccess {Number} incidents.locationID Location identifier
 * @apiSuccess {String} incidents.headline Headline for the incident
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
	"incidents": [
        {
            "id": 1,
            "locationID": 1,
            "headline": "Fejl på central switch",
            "createBy": 1,
            "createTime": "2018-10-18 20:49:50",
            "changeBy": null,
            "changeTime": null,
        }
    ]
}
 *
 * @apiUse NotFoundException
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsOpen}/locations/{locationID:[0-9]+}/incidents", function( $locationID ) use ( $app ) {

	$responseData = [];

	$limit    = $app->request->getQuery( 'limit', null, '0' );
	$offset   = $app->request->getQuery( 'offset', null, '0' );
	$fromTime = date( 'Y-m-d H:i:s', $app->request->getQuery( 'from-time', null, '0' ) );
	$toTime   = date( 'Y-m-d H:i:s', $app->request->getQuery( 'to-time', null, time() ) );

	// Check if location exists
	if( models\Locations::findFirst( $locationID ) === false )
		throw new exceptions\ObjectNotFoundException( 'location' );

	$incidents = models\Incidents::find(
		[
			'conditions' => 'locationID = ?1 AND createTime BETWEEN ?2 AND ?3',
			'bind'       => [
				1 => $locationID,
				2 => $fromTime,
				3 => $toTime
			],
			'limit'      => $limit,
			'offset'     => $offset
		]
	);
	if( $incidents !== false ) {

		$total                     = count( models\Incidents::find( [ "locationID = {$locationID}" ] ) );
		$responseData['_meta']     = [ 'itemCount' => count( $incidents ), 'itemsTotal' => $total ];
		$responseData['incidents'] = $incidents;
	}
	else {

		$responseData['_meta']     = [ 'itemCount' => 0, 'itemsTotal' => 0 ];
		$responseData['incidents'] = [];
	}

	$response = new JSONResponse( 200, $responseData );
	$response->send();
}
);

/**
 * @api {get} v1/open/incident-status List of incident status
 * @apiName IncidentStatusList
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Open
 *
 * @apiDescription
 *
 * Returns a list containing all incident status
 *
 * @apiSuccess {Array} incidentStatus Incident status list
 * @apiSuccess {Number} incidentStatus.id Incident status identifier
 * @apiSuccess {Number} incidentStatus.name Web-friendly name for the status
 * @apiSuccess {String="OK","Warning","Danger"} incidentStatus.severityLevel Severity of the incident.
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
    "incidentStatus": [
        {
            "id": "1",
            "name": "Kritisk",
            "severityLevel": "Danger",
            "createBy": "1",
            "createTime": "2018-10-18 20:49:30",
            "changeBy": null,
            "changeTime": null
        },
        {
            "id": "2",
            "name": "Advarsel",
            "severityLevel": "Warning",
            "createBy": "1",
            "createTime": "2018-10-18 20:49:30",
            "changeBy": null,
            "changeTime": null
        },
        {
            "id": "3",
            "name": "Løst",
            "severityLevel": "OK",
            "createBy": "1",
            "createTime": "2018-10-18 20:49:30",
            "changeBy": null,
            "changeTime": "2018-10-25 15:52:53"
        }
    ]
}
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsOpen}/incident-status", function() use ( $app ) {

	$responseData = [];

	$limit  = $app->request->getQuery( 'limit', null, '0' );
	$offset = $app->request->getQuery( 'offset', null, '0' );

	$incidentStatus = models\IncidentStatus::find(
		[
			'showOnWeb = 1',
			'limit' => $limit,
			'offset' => $offset,
			'columns' => 'id, name, severityLevel, createBy, createTime, changeBy, changeTime'
		]
	);
	if( $incidentStatus !== false ) {


		$total                          = models\IncidentStatus::count();
		$responseData['_meta']          = [ 'itemCount' => count( $incidentStatus ), 'itemsTotal' => $total ];
		$responseData['incidentStatus'] = $incidentStatus;
	}
	else {

		$responseData['_meta']          = [ 'itemCount' => 0, 'itemsTotal' => 0 ];
		$responseData['incidentStatus'] = [];
	}

	$response = new JSONResponse( 200, $responseData );
	$response->send();
}
);

/**
 * @api {get} v1/open/incident-status Specific incident status
 * @apiName IncidentStatus
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Open
 *
 * @apiDescription
 *
 * Returns the requested incident status
 *
 * @apiSuccess {Object} incidentStatus Incident status object
 * @apiSuccess {Number} incidentStatus.id Incident status identifier
 * @apiSuccess {Number} incidentStatus.name Web-friendly name for the status
 * @apiSuccess {String="OK","Warning","Danger"} incidentStatus.severityLevel Severity of the incident.
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
    "incidentStatus": {
            "id": "1",
            "name": "Kritisk",
            "severityLevel": "Danger",
            "createBy": "1",
            "createTime": "2018-10-18 20:49:30",
            "changeBy": null,
            "changeTime": null
        }
    }
}
 *
 * @apiUse NotFoundException
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsOpen}/incident-status/{id:[0-9]+}", function( $id ) {

	$responseData = [];

	$incidentStatus = models\IncidentStatus::findFirst(
		[
			'columns' => 'id, name, severityLevel, createBy, createTime, changeBy, changeTime',
			'conditions' => 'id = ?1 AND showOnWeb = 1',
			'bind' => [
				1 => $id
			]
		]
	);
	if( $incidentStatus !== false )
		$responseData['incidentStatus'] = $incidentStatus;
	else
		throw new exceptions\ObjectNotFoundException( 'incident status' );

	$response = new JSONResponse( 200, $responseData );
	$response->send();
}
);
