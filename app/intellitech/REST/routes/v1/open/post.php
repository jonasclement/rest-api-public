<?php

use intellitech\REST\Responses\v1\EmptyResponse;
use intellitech\REST\Responses\v1\JSONResponse;
use intellitech\lib\v1\Namespaces;
use intellitech\lib\v1\Tools;
use intellitech\lib\v1\ADComm;
use intellitech\models\v1 as models;
use intellitech\REST\Exceptions\v1 as exceptions;

// Namespaces for building routes
$nsOpen       = Namespaces::OPEN;
$nsRestricted = Namespaces::RESTRICTED;
$nsSecret     = Namespaces::SECRET;

/**
 * @api {post} v1/open/user-sessions Create user session
 * @apiName CreateUserSession
 * @apiGroup Users
 * @apiVersion 1.0.0
 * @apiPermission Open
 *
 * @apiDescription
 *
 * Authenticate a user and generate a token
 *
 * @apiParam {String} username Username to authenticate
 * @apiParam {String} password Password to authenticate in plain text
 * @apiParamExample {json} Request-Body-Example
{
	"username": "test",
	"password": "hest_med_v3st"
}
 *
 * @apiSuccess {Object} token Object containing the generated token data
 * @apiSuccess {String} token.selector Token selector
 * @apiSuccess {String} token.token Base64-encoded token
 * @apiSuccess {String} token.expires YYYY-MM-DD HH:mm:ss timestamp indicating when the token will expire
 *
 * @apiSuccessExample {json} Success-201-Example
HTTP/1.1 201 Created
{
    "token": {
        "selector": "bfOdjFpsfcnE",
        "token": "dP5uf0Ye5wEQxQ6U441TIPHY+XiwqHSYkDRx9cC2HaOY",
        "expires": "2018-10-25 17:07:11"
    }
}
 *
 * @apiUse NotFoundException
 */
$app->post(
	"{$version->getBasePrefix()}/{$nsOpen}/user-sessions", function() {

	$username = $_POST['username'] ?? '';
	$password = $_POST['password'] ?? '';

	if( empty( $username ) || empty( $password ) )
		throw new exceptions\BadRequestException( 'Missing fields in request body' );

	// Create a login attempt
	$loginAttempt            = new models\LoginAttempts();
	$loginAttempt->sourceIP  = $_SERVER['REMOTE_ADDR'];
	$loginAttempt->username  = $username;
	$loginAttempt->sessionID = '0';

	// Authenticate with active directory
	$ad   = new ADComm();
	$auth = $ad->authenticate( $username, $password );

	if( $auth ) {

		// Find user in the AD
		$adUser = $ad->getUserDataByUsername( $username );
		if( $adUser === false )
			throw new exceptions\InternalServerErrorException( 'Something went terribly wrong, go punch an active directory in the nose' ); // Should never happen since we've just authenticated...

		// Find user
		$user = models\Users::findFirstByAdDN( $adUser->getDn() );
		if( $user !== false ) {

			$loginAttempt->userID  = $user->id;
			$loginAttempt->success = 1;
			if( $loginAttempt->save() ) {

				$sessionData = Tools::createNewUserSession( $user, $adUser, $loginAttempt );
				$response    = new JSONResponse(
					201,
					[
						'token' =>
							[
								'selector' => $sessionData['selector'],
								'token'    => base64_encode( $sessionData['token'] ),
								'expires'  => $sessionData['expires']
							]
					]
				);
				$response->send();
			}
			else
				throw new exceptions\InternalServerErrorException( 'Something went wrong during authentication.' );
		}
		else {
			throw new exceptions\InternalServerErrorException( "The user exists in AD, but was not found in our database. DN: {$adUser->getDn()}" );
		}
	}
	else {

		$loginAttempt->success = 0;
		$loginAttempt->save();
		throw new exceptions\ObjectNotFoundException( 'user' );
	}
}
);
