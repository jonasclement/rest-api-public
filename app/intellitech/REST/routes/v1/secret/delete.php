<?php

use intellitech\REST\Responses\v1\EmptyResponse;
use intellitech\lib\v1\Namespaces;
use intellitech\models\v1 as models;
use intellitech\REST\Exceptions\v1 as exceptions;

// Namespaces for building routes
$nsOpen       = Namespaces::OPEN;
$nsRestricted = Namespaces::RESTRICTED;
$nsSecret     = Namespaces::SECRET;

/**
 * @api {delete} v1/secret/incidents/<id> Delete incident
 * @apiName DeleteIncident
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Delete an incident and all associated messages
 *
 * @apiSuccessExample {json} Success-204-Example
HTTP/1.1 204 No Content
 *
 * @apiUse NotFoundException
 */
$app->delete(
	"{$version->getBasePrefix()}/{$nsSecret}/incidents/{id:[0-9]+}", function( $id ) {

	$incident = models\Incidents::findFirst( $id );
	if( $incident !== false ) {

		// First, delete all lines
		$messages = models\IncidentLines::find( [ "incidentID = {$incident->id}" ] );
		if( $messages !== false ) {

			foreach( $messages as $message ) {

				$message->delete();
			}
		}

		// Delete the incident
		if( $incident->delete() ) {

			$response = new EmptyResponse( 204 );
			$response->send();
		}
		else
			throw new exceptions\InternalServerErrorException( 'Something went wrong while deleting the incident' );
	}
	else
		throw new exceptions\ObjectNotFoundException( 'incident' );
}
);

/**
 * @api {delete} v1/secret/incidents/<incident-id>/messages/<id> Delete incident message
 * @apiName DeleteIncidentMessage
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Deletes the requested incident message
 *
 * @apiSuccessExample {json} Success-204-Example
HTTP/1.1 204 No Content
 *
 * @apiUse NotFoundException
 * @apiUse UnprocessableEntityException
 */
$app->delete(
	"{$version->getBasePrefix()}/{$nsSecret}/incidents/{incidentID:[0-9]+}/messages/{id:[0-9]+}", function( $incidentID, $id ) {

	$incident = models\Incidents::findFirst($incidentID);
	if( $incident !== false ) {

		$message = models\IncidentLines::findFirst( $id );
		if( $message !== false ) {

			if( $message->delete() ) {

				$response = new EmptyResponse( 204 );
				$response->send();
			}
			else {
				throw new exceptions\InternalServerErrorException( 'Something went wrong while deleting the incident message' );
			}
		}
		else
			throw new exceptions\ObjectNotFoundException( 'incident message' );
	}
	else
		throw new exceptions\UnprocessableEntityException('incident');
}
);
