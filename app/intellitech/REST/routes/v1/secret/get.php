<?php

use intellitech\REST\Responses\v1\JSONResponse;
use intellitech\lib\v1\Namespaces;
use intellitech\models\v1 as models;
use intellitech\REST\Exceptions\v1 as exceptions;
use intellitech\lib\v1\ADComm;
use intellitech\lib\v1\Tools;

// Namespaces for building routes
$nsOpen       = Namespaces::OPEN;
$nsRestricted = Namespaces::RESTRICTED;
$nsSecret     = Namespaces::SECRET;

/**
 * @api {get} v1/secret/login-attempts List of login attempts
 * @apiName LoginAttemptList
 * @apiGroup LoginAttempts
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Returns an array of containing all login attempts.
 *
 * @apiSuccess {Array} loginAttempts Login attempt list
 * @apiSuccess {Number} loginAttempts.id Login attempt identifier
 * @apiSuccess {Number} loginAttempts.userID User identifier
 * @apiSuccess {Number} loginAttempts.username Attempted username
 * @apiSuccess {String} loginAttempts.sourceIP Source IP of the login
 * @apiSuccess {String} loginAttempts.sessionID Session ID (currently unused)
 * @apiSuccess {Boolean} loginAttempts.success Whether or not the login was successful
 * @apiSuccess {String} loginAttempts.createTime Timestamp for when the attempt was made
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
"loginAttempts": [
	{
		"id": 1,
		"userID": 1,
		"username": "test",
		"sourceIP": "127.0.0.1",
		"sessionID": "0",
		"success": 1,
		"createTime": "2018-10-24 15:13:19"
	},
	{
		"id": 2,
		"userID": 1,
		"username": "test",
		"sourceIP": "127.0.0.1",
		"sessionID": "0",
		"success": 1,
		"createTime": "2018-10-24 15:13:25"
	},
	{
		"id": 3,
		"userID": 2,
		"username": "hest",
		"sourceIP": "127.0.0.1",
		"sessionID": "0",
		"success": 1,
		"createTime": "2018-10-24 15:13:35"
	}
]
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsSecret}/login-attempts", function() use ( $app ) {

	$responseData = [];

	$limit    = $app->request->getQuery( 'limit', null, '0' );
	$offset   = $app->request->getQuery( 'offset', null, '0' );
	$fromTime = date( 'Y-m-d H:i:s', $app->request->getQuery( 'from-time', null, '0' ) );
	$toTime   = date( 'Y-m-d H:i:s', $app->request->getQuery( 'to-time', null, time() ) );

	$loginAttempts = models\LoginAttempts::find(
		[
			'conditions' => 'createTime BETWEEN ?1 AND ?2',
			'bind'       => [
				1 => $fromTime,
				2 => $toTime
			],
			'limit'      => $limit,
			'offset'     => $offset
		]
	);
	if( $loginAttempts !== false ) {

		$total                         = models\LoginAttempts::count(
			[
				'conditions' => 'createTime BETWEEN ?1 AND ?2',
				'bind'       => [
					1 => $fromTime,
					2 => $toTime
				]
			]
		);
		$responseData['_meta']         = [ 'itemCount' => count( $loginAttempts ), 'itemsTotal' => $total ];
		$responseData['loginAttempts'] = $loginAttempts;
	}
	else {

		$responseData['_meta']         = [ 'itemCount' => 0, 'itemsTotal' => 0 ];
		$responseData['loginAttempts'] = [];
	}

	$response = new JSONResponse( 200, $responseData );
	$response->send();
}
);

/**
 * @api {get} v1/secret/users/<user-id>/login-attempts List of login attempts by user
 * @apiName LoginAttemptListByUser
 * @apiGroup LoginAttempts
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Returns an array of containing all login attempts.
 *
 * @apiSuccess {Array} loginAttempts Login attempt list
 * @apiSuccess {Number} loginAttempts.id Login attempt identifier
 * @apiSuccess {Number} loginAttempts.userID User identifier
 * @apiSuccess {Number} loginAttempts.username Attempted username
 * @apiSuccess {String} loginAttempts.sourceIP Source IP of the login
 * @apiSuccess {String} loginAttempts.sessionID Session ID (currently unused)
 * @apiSuccess {Boolean} loginAttempts.success Whether or not the login was successful
 * @apiSuccess {String} loginAttempts.createTime Timestamp for when the attempt was made
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
"loginAttempts": [
	{
		"id": 1,
		"userID": 1,
		"username": "test",
		"sourceIP": "127.0.0.1",
		"sessionID": "0",
		"success": 1,
		"createTime": "2018-10-24 15:13:19"
	},
	{
		"id": 2,
		"userID": 1,
		"username": "test",
		"sourceIP": "127.0.0.1",
		"sessionID": "0",
		"success": 1,
		"createTime": "2018-10-24 15:13:25"
	}
]
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsSecret}/users/{userID:[0-9]+}/login-attempts", function( $userID ) use ( $app ) {

	$responseData = [];

	$limit  = $app->request->getQuery( 'limit', null, '0' );
	$offset = $app->request->getQuery( 'offset', null, '0' );

	$user = models\Users::findFirst( $userID );
	if( $user === false )
		throw new exceptions\ObjectNotFoundException( 'user' );

	$loginAttempts = models\LoginAttempts::find( [ "userID = {$userID}", 'limit' => $limit, 'offset' => $offset ] );
	if( $loginAttempts !== false ) {

		$total                         = models\LoginAttempts::count();
		$responseData['_meta']         = [ 'itemCount' => count( $loginAttempts ), 'itemsTotal' => $total ];
		$responseData['loginAttempts'] = $loginAttempts;
	}
	else {

		$responseData['_meta']         = [ 'itemCount' => 0, 'itemsTotal' => 0 ];
		$responseData['loginAttempts'] = [];
	}

	$response = new JSONResponse( 200, $responseData );
	$response->send();
}
);

/**
 * @api {get} v1/secret/login-attempts/<id> Specific login attempt
 * @apiName LoginAttempt
 * @apiGroup LoginAttempts
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Returns the requested login attempt.
 *
 * @apiSuccess {Object} loginAttempt Login attempt object
 * @apiSuccess {Number} loginAttempts.id Login attempt identifier
 * @apiSuccess {Number} loginAttempts.userID User identifier
 * @apiSuccess {Number} loginAttempts.username Attempted username
 * @apiSuccess {String} loginAttempts.sourceIP Source IP of the login
 * @apiSuccess {String} loginAttempts.sessionID Session ID (currently unused)
 * @apiSuccess {Boolean} loginAttempts.success Whether or not the login was successful
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
"loginAttempt": {
	"id": 1,
	"userID": 1,
	"username": "test",
	"sourceIP": "127.0.0.1",
	"sessionID": "0",
	"success": 1,
}

 */
$app->get(
	"{$version->getBasePrefix()}/{$nsSecret}/login-attempts/{id:[0-9]+}", function( $id ) {

	$responseData = [];

	$loginAttempt = models\LoginAttempts::findFirst( $id );
	if( $loginAttempt !== false )
		$responseData['loginAttempt'] = $loginAttempt;
	else
		throw new exceptions\ObjectNotFoundException( 'login attempt' );

	$response = new JSONResponse( 200, $responseData );
	$response->send();
}
);

/**
 * @api {get} v1/secret/applications List of API applications
 * @apiName APIApplicationList
 * @apiGroup Applications
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Returns a list containing all API applications
 *
 * @apiSuccess {Array} applications Application list
 * @apiSuccess {Number} applications.id Application identifier
 * @apiSuccess {Number} applications.name Web-friendly name for the application
 * @apiSuccess {Number} applications.appID Application ID used for authentication
 * @apiSuccess {String} applications.secret Application secret used for authentication. Must be kept, well... secret.
 * @apiSuccess {Boolean} applications.allowUse Whether or not the application is allowed to use the API
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
"applications": [
        {
            "id": 1,
            "name": "Web app",
            "appID": "10000001",
            "secret": "80b13ebdce58a567d5fbbd39a2f3748c7996898567e452e82f57097ad232b4f7",
            "allowUse": 1,
            "createBy": 1,
            "createTime": "2018-10-26 13:56:30",
            "changeBy": null,
            "changeTime": null
        },
        {
            "id": 2,
            "name": "Mobil app",
            "appID": "10000002",
            "secret": "101cbb0685d593090a73025352b4cd7da9162681af230a739053f40d3b04c2c2",
            "allowUse": 1,
            "createBy": 1,
            "createTime": "2018-10-26 13:56:30",
            "changeBy": null,
            "changeTime": null
        }
    ]
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsSecret}/applications", function() use( $app ) {

	$responseData = [];

	$limit  = $app->request->getQuery( 'limit', null, '0' );
	$offset = $app->request->getQuery( 'offset', null, '0' );

	$applications = models\APIApplications::find( [ 'limit' => $limit, 'offset' => $offset ] );
	if( $applications !== false ) {

		$total                         = models\APIApplications::count();
		$responseData['_meta']         = [ 'itemCount' => count( $applications ), 'itemsTotal' => $total ];
		$responseData['applications'] = $applications;
	}
	else {

		$responseData['_meta']          = [ 'itemCount' => 0, 'itemsTotal' => 0 ];
		$responseData['applications'] = [];
	}

	$response = new JSONResponse( 200, $responseData );
	$response->send();
}
);

/**
 * @api {get} v1/secret/applications/<id> Specific API application
 * @apiName APIApplication
 * @apiGroup Applications
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Returns an object containing the requested API application
 *
 * @apiSuccess {Object} application Application object
 * @apiSuccess {Number} application.id Application identifier
 * @apiSuccess {Number} application.name Web-friendly name for the application
 * @apiSuccess {Number} application.appID Application ID used for authentication
 * @apiSuccess {String} application.secret Application secret used for authentication. Must be kept, well... secret.
 * @apiSuccess {Boolean} application.allowUse Whether or not the application is allowed to use the API
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
    "application": {
        "id": 1,
        "name": "Web app",
        "appID": "10000001",
        "secret": "80b13ebdce58a567d5fbbd39a2f3748c7996898567e452e82f57097ad232b4f7",
        "allowUse": 1,
        "createBy": 1,
        "createTime": "2018-10-26 13:56:30",
        "changeBy": null,
        "changeTime": null
    }
}
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsSecret}/applications/{id:[0-9]+}", function( $id ) {

	$responseData = [];

	$application = models\APIApplications::findFirst( $id );
	if( $application !== false )
		$responseData['application'] = $application;
	else
		throw new exceptions\ObjectNotFoundException( 'application' );

	$response = new JSONResponse( 200, $responseData );
	$response->send();
}
);

/**
 * @api {get} v1/secret/users List of users
 * @apiName Users
 * @apiGroup Users
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Returns an array containing users.
 *
 * @apiSuccess {Array} users User list
 * @apiSuccess {Number} users.id User identifier
 * @apiSuccess {Number} users.locationID User location identifier
 * @apiSuccess {String} users.username User's username
 * @apiSuccess {String} users.displayName User's display name,
 * @apiSuccess {Boolean} users.isAdmin Whether the user is an administrator or not
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
    "users": [
        {
            "id": 1,
            "locationID": 1,
            "username": "fko-user01",
            "displayName": "fko normaluser 01",
            "isAdmin": false
        },
        {
            "id": 2,
            "locationID": null,
            "username": "fko-webadmin",
            "displayName": "Fko webadmin",
            "isAdmin": true
        }
    ]
}
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsSecret}/users", function() use ( $app ) {

	$responseData = [];

	$limit  = $app->request->getQuery( 'limit', null, '0' );
	$offset = $app->request->getQuery( 'offset', null, '0' );

	$users = models\Users::find( [ 'limit' => $limit, 'offset' => $offset ] );
	if( $users !== false ) {

		$total                 = models\Users::count();
		$responseData['_meta'] = [ 'itemCount' => count( $users ), 'itemsTotal' => $total ];
		$responseData['users'] = [];

		$adComm = new ADComm();
		foreach( $users as $user ) {

			// Fetch AD data
			$userAdData     = $adComm->getUserDataByDn( $user->adDN );
			$locationAdData = $adComm->getLocationByUserDN( $user->adDN );

			$responseRow = [
				'id'          => $user->id,
				'locationID'  => models\Locations::findFirstByAdDN( $locationAdData->getDn() )->id ?? null,
				'username'    => $userAdData->getAccountName(),
				'displayName' => $userAdData->getDisplayName(),
				'isAdmin'     => Tools::checkIfAdUserIsAdmin( $userAdData )
			];

			$responseData['users'][] = $responseRow;
		}

		$response = new JSONResponse( 200, $responseData );
		$response->send();
	}
	else {

		$responseData['_meta'] = [ 'itemCount' => 0, 'itemsTotal' => 0 ];
		$responseData['users'] = [];
	}
}
);

/**
 * @api {get} v1/secret/users/<id> User
 * @apiName User
 * @apiGroup Users
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Returns an object containing the requested user.
 *
 * @apiSuccess {Object} users User object
 * @apiSuccess {Number} user.id User identifier
 * @apiSuccess {Number} user.locationID User location identifier
 * @apiSuccess {String} user.username User's username
 * @apiSuccess {String} user.displayName User's display name,
 * @apiSuccess {Boolean} user.isAdmin Whether the user is an administrator or not
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
	"user": {
		"id": 1,
		"locationID": 1,
		"username": "fko-user01",
		"displayName": "fko normaluser 01",
		"isAdmin": false
	}
}
 */
$app->get(
	"{$version->getBasePrefix()}/{$nsSecret}/users/{id:[0-9]+}", function( $id ) {

	$responseData = [];

	$user = models\Users::findFirst( $id );
	if( $user !== false ) {

		$adComm = new ADComm();

		// Fetch AD data
		$userAdData     = $adComm->getUserDataByDn( $user->adDN );
		$locationAdData = $adComm->getLocationByUserDN( $user->adDN );

		$responseData['user'] =
			[
				'id'          => $user->id,
				'locationID'  => models\Locations::findFirstByAdDN( $locationAdData->getDn() )->id ?? null,
				'username'    => $userAdData->getAccountName(),
				'displayName' => $userAdData->getDisplayName(),
				'isAdmin'     => Tools::checkIfAdUserIsAdmin( $userAdData )
			];

		$response = new JSONResponse( 200, $responseData );
		$response->send();
	}
	else
		throw new exceptions\ObjectNotFoundException( 'user' );
}
);
