<?php

use intellitech\REST\Responses\v1\EmptyResponse;
use intellitech\lib\v1\Namespaces;
use intellitech\models\v1 as models;
use intellitech\REST\Exceptions\v1 as exceptions;
use intellitech\lib\v1\ADComm;

// Namespaces for building routes
$nsOpen       = Namespaces::OPEN;
$nsRestricted = Namespaces::RESTRICTED;
$nsSecret     = Namespaces::SECRET;

/**
 * @api {post} v1/secret/incidents Create incident
 * @apiName CreateIncident
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Create a new incident
 *
 * @apiParam {Number} locationID Location identifier
 * @apiParam {String} headline Headline for the incident
 * @apiParamExample {json} Request-Body-Example
{
	"locationID": 1,
	"headline": "Nedbrud på noget udstyr"
}
 *
 * @apiSuccessExample {json} Success-204-Example
HTTP/1.1 204 No Content
 *
 * @apiUse UnprocessableEntityException
 */
$app->post(
	"{$version->getBasePrefix()}/{$nsSecret}/incidents", function() use ( $version, $nsOpen ) {

	// Validate inputs
	$locationID = $_POST['locationID'] ?? '';
	$headline   = $_POST['headline'] ?? '';

	if( empty( $locationID ) || empty( $headline ) )
		throw new exceptions\BadRequestException( 'Missing fields in request body' );

	// Check if location exists
	$location = models\Locations::findFirst( $locationID );
	if( $location === false )
		throw new exceptions\UnprocessableEntityException( 'location' );

	// If nothing has gone wrong this far, create and insert the Incident
	$incident             = new models\Incidents();
	$incident->locationID = $locationID;
	$incident->headline   = $headline;
	$incident->createBy   = $_SESSION['authUser']->id;

	if( $incident->save() ) {

		$adComm = new ADComm();
		$locationAdData = $adComm->getOuDataByDN( $incident->Locations->adDN );

		// Send notification to installed clients at the location
		$serviceAccount = \Kreait\Firebase\ServiceAccount::fromJsonFile( APP_PATH . '/config/google-service-account.json' );
		$fb             = (new \Kreait\Firebase\Factory)
			->withServiceAccount( $serviceAccount )
			->create();

		$fb->getMessaging()->send(
			[
				'topic'        => "loc{$incident->locationID}",
				'notification' => [
					'title' => "Ny melding for {$locationAdData->getOu()}",
					'body'  => $incident->headline,
				],

				//	Android
				'android'      => [
					'ttl'          => '3600s',
					'priority'     => 'high',
					'notification' => [
						//'title' => 'Android-Specific text',
						//'body' => 'Android-specific text',
						'icon'  => 'stock_ticker_update',
						'color' => '#6c91bf',
					],
				],

				//	Apple (Apple Push Notification Service)
				//    currently commented out because we can't test Apple devices without paying up
				/*
				'apns' => [
					'headers' => [
						'apns-priority' => '10',
					],
					'payload' => [
						'aps' => [
							'alert' => [
								'title' => '$GOOG up 1.43% on the day',
								'body' => '$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
							],
							'badge' => 42,
						],
					],
				],
				*/
			]
		);

		$response = new EmptyResponse( 204 );
		$response->setHeader( 'Content-Location', "{$version->getBasePrefix()}/{$nsOpen}/incidents/{$incident->id}" );
		$response->send();
	}
	else
		throw new exceptions\InternalServerErrorException( 'Something went wrong while saving the incident' );
}
);

/**
 * @api {post} v1/secret/incidents/<incident-id>/messages Create incident message
 * @apiName CreateIncidentMessage
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Create a new incident message
 *
 * @apiParam {Number} incidentStatusID Incident status identifier
 * @apiParam {String} message Message
 * @apiParamExample {json} Request-Body-Example
{
	"incidentStatusID": 2,
	"message": "Some update text"
}
 *
 * @apiSuccessExample {json} Success-204-Example
HTTP/1.1 204 No Content
 *
 * @apiUse UnprocessableEntityException
 */
$app->post(
	"{$version->getBasePrefix()}/{$nsSecret}/incidents/{incidentID:[0-9]+}/messages", function( $incidentID ) use ( $version, $nsOpen ) {

	// Validate inputs
	$incidentStatusID = $_POST['incidentStatusID'] ?? '';
	$message          = $_POST['message'] ?? '';

	if( empty( $incidentID ) || empty( $message ) )
		throw new exceptions\BadRequestException( 'Missing fields in request body' );

	// Check if the incident exists
	$incident = models\Incidents::findFirst( $incidentID );
	if( $incident === false )
		throw new exceptions\UnprocessableEntityException( 'incident' );

	// Check if the incident status exists
	$incidentStatus = models\IncidentStatus::findFirst( $incidentStatusID );
	if( $incidentStatus === false )
		throw new exceptions\UnprocessableEntityException( 'incident status' );

	// Nothing has gone wrong this far, so create and insert the message
	$incidentLine                   = new models\IncidentLines();
	$incidentLine->incidentID       = $incident->id;
	$incidentLine->incidentStatusID = $incidentStatus->id;
	$incidentLine->message          = $message;
	$incidentLine->createBy			= $_SESSION['authUser']->id;

	if( $incidentLine->save() ) {

		$response = new EmptyResponse( 204 );
		$response->setHeader( 'Content-Location', "{$version->getBasePrefix()}/{$nsOpen}/incidents/{$incident->id}/messages/{$incidentLine->id}" );
		$response->send();
	}
	else
		throw new exceptions\InternalServerErrorException( 'Something went wrong while saving the message' );
}
);

/**
 * @api {post} v1/secret/applications Create API application
 * @apiName CreateAPIApplication
 * @apiGroup Applications
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Create a new API application
 *
 * @apiParam {String{1..45}} name Name for the application
 * @apiParam {Number{10000000-99999999}} appID Application ID for auth
 * @apiParam {Boolean} allowUse Whether or not the application can access the API
 * @apiParamExample {json} Request-Body-Example
{
	"name": "Test app, please ignore",
	"appID": 10000003,
	"allowUse": false
}
 *
 * @apiSuccessExample {json} Success-204-Example
HTTP/1.1 204 No Content
 */
$app->post(
	"{$version->getBasePrefix()}/{$nsSecret}/applications", function() use ( $version, $nsSecret ) {

	// Validate inputs
	$name     = $_POST['name'] ?? '';
	$appID    = $_POST['appID'] ?? '';
	$allowUse = $_POST['allowUse'] ?? '';

	if( empty( $name ) || empty( $appID ) || !is_bool( $allowUse ) )
		throw new exceptions\BadRequestException( 'Missing fields in request body' );

	// Name validation
	if( strlen( $name ) > 45 )
		throw new exceptions\BadRequestException( 'Parameter "name" is too long' );
	if( models\APIApplications::findFirstByName( $name ) !== false )
		throw new exceptions\BadRequestException( 'Parameter "name" is already in use' );

	// App ID validation
	if( strlen( $appID ) > 8 )
		throw new exceptions\BadRequestException( 'Parameter "appID" is too long' );
	if( !is_numeric( $appID ) )
		throw new exceptions\BadRequestException( 'Parameter "appID" is invalid' );
	if( models\APIApplications::findFirstByAppID( $appID ) !== false )
		throw new exceptions\BadRequestException( 'Parameter "appID" is already in use' );

	// Nothing has gone wrong, so generate a secret and insert the application
	$application           = new models\APIApplications();
	$application->name     = $name;
	$application->appID    = $appID;
	$application->secret   = hash( 'sha256', $name . $appID . microtime() );
	$application->allowUse = $allowUse;
	$application->createBy = $_SESSION['authUser']->id;
	if( $application->save() ) {

		$response = new EmptyResponse( 204 );
		$response->setHeader( 'Content-Location', "{$version->getBasePrefix()}/{$nsSecret}/applications/{$application->id}" );
		$response->send();
	}
	else
		throw new exceptions\InternalServerErrorException( 'Something went wrong while saving the application' );
}
);
