<?php

use intellitech\REST\Responses\v1\EmptyResponse;
use intellitech\lib\v1\Namespaces;
use intellitech\models\v1 as models;
use intellitech\REST\Exceptions\v1 as exceptions;

// Namespaces for building routes
$nsOpen       = Namespaces::OPEN;
$nsRestricted = Namespaces::RESTRICTED;
$nsSecret     = Namespaces::SECRET;

/**
 * @api {patch} v1/secret/incidents/<id> Update an incident
 * @apiName UpdateIncident
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Changes the requested incident
 *
 * @apiParam {Number} locationID Location identifier
 * @apiParam {String} headline New headline for the incident
 * @apiParamExample {json} Request-Body-Example
{
	"locationID": 1
	"headline": "Same headline but a typo is fixed"
}
 *
 * @apiSuccessExample {json} Success-204-Example
HTTP/1.1 204 No Content
 *
 * @apiUse NotFoundException
 * @apiUse UnprocessableEntityException
 */
$app->patch(
	"{$version->getBasePrefix()}/{$nsSecret}/incidents/{id:[0-9]+}", function( $id ) use ( $version, $nsOpen ) {

	$incident = models\Incidents::findFirst( $id );
	if( $incident !== false ) {

		// Check if location exists
		if( isset( $_POST['locationID'] ) && models\Locations::findFirst( $_POST['locationID'] ) === false )
			throw new exceptions\UnprocessableEntityException( 'location' );

		// Update the incident
		$incident->locationID = $_POST['locationID'] ?? $incident->locationID;
		$incident->headline   = $_POST['headline'] ?? $incident->headline;
		$incident->changeBy   = $_SESSION['authUser']->id;
		if( $incident->save() ) {

			$response = new EmptyResponse( 204 );
			$response->setHeader( 'Content-Location', "{$version->getBasePrefix()}/{$nsOpen}/incidents/{$incident->id}" );
			$response->send();
		}
		else
			throw new exceptions\InternalServerErrorException( 'Something went wrong while updating the Incident.' );
	}
	else
		throw new exceptions\ObjectNotFoundException( 'incident' );
}
);

/**
 * @api {patch} v1/secret/incidents/<incident-id>/messages/<id> Update incident message
 * @apiName UpdateIncidentMessage
 * @apiGroup Incidents
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Changes the requested incident message
 *
 * @apiParam {Number} incidentStatusID Incident status identifier
 * @apiParam {String} message Message
 * @apiParamExample {json} Request-Body-Example
{
	"incidentStatusID": 1,
	"message": "Some changed text"
}
 *
 * @apiSuccessExample {json} Success-204-Example
HTTP/1.1 204 No Content
 *
 * @apiUse NotFoundException
 * @apiUse UnprocessableEntityException
 */
$app->patch(
	"{$version->getBasePrefix()}/{$nsSecret}/incidents/{incidentID:[0-9]+}/messages/{id:[0-9]+}", function( $incidentID, $id ) use ( $version, $nsOpen ) {

	$incident = models\Incidents::findFirst( $incidentID );
	if( $incident !== false ) {

		$message = models\IncidentLines::findFirst( $id );
		if( $message !== false && $message->incidentID == $incident->id ){

			// Update the message
			$message->incidentStatusID = $_POST['incidentStatusID'] ?? $message->incidentStatusID;
			$message->message = $_POST['message'] ?? $message->message;
			$message->changeBy = $_SESSION['authUser']->id;
			if( $message->save() ) {

				$response = new EmptyResponse( 204 );
				$response->setHeader( 'Content-Location', "{$version->getBasePrefix()}/{$nsOpen}/incidents/{$incident->id}/messages/{$message->id}" );
				$response->send();
			}
			else
				throw new exceptions\InternalServerErrorException( 'Something went wrong while updating the Incident Message' );
		}
		else
			throw new exceptions\ObjectNotFoundException( 'incident message' );
	}
	else
		throw new exceptions\UnprocessableEntityException( 'incident' );
}
);

/**
 * @api {patch} v1/secret/applications/<id> Update API application
 * @apiName UpdateAPIApplication
 * @apiGroup Applications
 * @apiVersion 1.0.0
 * @apiPermission Secret
 *
 * @apiDescription
 *
 * Update an API application
 *
 * @apiParam {Boolean} allowUse Whether or not the application can access the API
 * @apiParamExample {json} Request-Body-Example
{
	"allowUse": false
}
 *
 * @apiSuccessExample {json} Success-204-Example
HTTP/1.1 204 No Content
 *
 * @apiUse NotFoundException
 */
$app->patch(
	"{$version->getBasePrefix()}/{$nsSecret}/applications/{id:[0-9]+}", function( $id ) use ( $version, $nsSecret ) {

	$application = models\APIApplications::findFirst( $id );
	if( $application !== false ) {

		// Update the application
		$application->allowUse = $_POST['allowUse'] ?? $application->allowUse;
		$application->changeBy = $_SESSION['authUser']->id;
		if( $application->save() ) {

			$response = new EmptyResponse( 204 );
			$response->setHeader( 'Content-Location', "{$version->getBasePrefix()}/{$nsSecret}/applications/{$application->id}" );
			$response->send();
		}
		else
			throw new exceptions\InternalServerErrorException( 'Something went wront while updating the Application' );
	}
	else
		throw new exceptions\ObjectNotFoundException( 'application' );
}
);
