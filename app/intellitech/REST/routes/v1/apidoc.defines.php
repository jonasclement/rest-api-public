<?php
/**
 * @apiDefine Open Open for public
 * This function does not require any authentication
 */
/**
 * @apiDefine Restricted Requires authenticated user
 * This function requires a token from an authenticated user, an app ID, and requires the request to be signed.
 */
/**
 * @apiDefine Secret Requires authenticated admin user
 * This function requires a token from an authenticated admin user, an app ID, and requires the request to be signed.
 */


/**
 * @apiVersion 1.0.0
 * @api {get} v1/resource Authentication headers
 * @apiName AuthenticationHeaders
 * @apiGroup General
 *
 * @apiDescription
 *
 * "Restricted" and "Secret" endpoints require authentication in the form of a user token. They also require the request to be signed.
 * For instructions on request signing, please see the instructions that were given to you with your App ID.
 * See <a href="#api-Users-CreateUserSession">Create user session</a> for information on generating tokens.
 *
 * @apiHeader (Authentication) {String} X-Auth-Token-Selector Token selector
 * @apiHeader (Authentication) {String} X-Auth-Token-Hash Token hash
 * @apiHeader (Authentication) {Number} X-App-ID Application identifier, given to your administrator
 */

/**
 * @apiVersion 1.0.0
 * @api {post} v1/resource Content-Location
 * @apiName ContentLocationHeader
 * @apiGroup General
 *
 * @apiDescription
 *
 * The Content-Location header is set in responses to every POST and PATCH request, containing a URL where the resource can be found with a GET request.
 * There is however one exception: <a href="#api-Users-CreateUserSession">Create user session</a> returns the generated token in the response body.
 *
 * @apiHeaderExample {String} Content-Location-Example
Content-Location: /api/v1/resource/4
 */

/**
 * @apiVersion 1.0.0
 * @api {get} v1/resource Content-Type
 * @apiName ContentTypeHeader
 * @apiGroup General
 *
 * @apiDescription
 *
 * The Content-Type header is required for every request. Currently we only support "application/json".
 *
 * @apiHeaderExample {String} Content-Type-Example
Content-Type: application/json
 */

/**
 * @apiVersion 1.0.0
 * @api {get} v1/resource Create/Change metadata
 * @apiName CreateAndChangeMetaData
 * @apiGroup General
 *
 * @apiDescription
 *
 * Create/change by and time metadata is present on most objects.
 *
 * @apiSuccess {Object} object Object being requested
 * @apiSuccess {Number} object.createBy ID of the user who created the object
 * @apiSuccess {String} object.createTime Timestamp formatted as YYYY-MM-DD HH:mm:ss showing when the object was created
 * @apiSuccess {Number} object.changeBy ID of the user who last changed the object
 * @apiSuccess {String} object.changeTime Timestamp formatted as YYYY-MM-DD HH:mm:ss showing when the object was last changed
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
    "object": {
    	"createBy": 1,
    	"createTime": "2018-10-18 21:53:34",
    	"changeBy": null,
    	"changeTime": null
    }
}
 */

/**
 * @apiVersion 1.0.0
 * @api {get} v1/resource?from-time=1540818345&to-time=1540818845 FromTime/ToTime
 * @apiName FromTimeToTime
 * @apiGroup General
 *
 * @apiDescription
 *
 * Some list endpoints (incidents, incidents by location, and login attempts) support adding from-time and to-time parameters.
 * This will make the response only contain objects created within that period.
 * Both parameters may be send independently.
 */

/**
 * @apiVersion 1.0.0
 * @api {get} v1/resource?<offset>=5&<limit>=100 Limit/Offset
 * @apiName LimitAndOffset
 * @apiGroup General
 *
 * @apiParam {Number{1-2500}} [limit=10] Limit records
 * @apiParam {Number} [offset=0] Offset of records
 *
 * @apiDescription
 *
 * Gives the possibility to limit and offset records returned
 *
 * @apiSuccess {Object} _meta Object containing request metadata
 * @apiSuccess {Number} _meta.itemCount Number of records returned
 * @apiSuccess {Number} _meta.totalItems Total number of records found
 *
 * @apiSuccessExample {json} Success-200-Example
HTTP/1.1 200 OK
{
	"_meta": {
 		"itemCount": 100,
		"totalItems": 1337
	}
}
 */

