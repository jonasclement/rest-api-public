<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader
	->registerNamespaces(
		[
			'intellitech\models\v1'       => $config->application->modelsDir,
			'intellitech\lib'             => $config->application->libsDir,
			'intellitech\REST\Exceptions' => $config->application->restExceptionsDir,
			'intellitech\REST\Responses'  => $config->application->restResponsesDir,
		]
	)
	->registerFiles(
		[
			APP_PATH . '/intellitech/lib/v1/vendor/autoload.php',
		]
	)
	->register();
