<?php
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

$dbpass = trim( file_get_contents( APP_PATH . '/config/db_pass.txt' ) );

return new \Phalcon\Config([
    'database' => [
        'adapter'    => 'Mysql',
        'host'       => '127.0.0.1',
        'username'   => 'intellitech',
        'password'   => $dbpass,
        'dbname'     => 'intellitech',
        'charset'    => 'utf8',
		'useSSL'   => true,
    ],

    'application' => [
		'modelsDir'       => APP_PATH . '/models/v1/',
		'migrationsDir'     => APP_PATH . '/migrations/',
		'viewsDir'          => APP_PATH . '/views/',
		'libsDir'           => APP_PATH . '/intellitech/lib/',
		'restExceptionsDir' => APP_PATH . '/intellitech/REST/exceptions/',
		'restResponsesDir'  => APP_PATH . '/intellitech/REST/responses/',
		'baseUri'           => '/api/',
    ]
]);
